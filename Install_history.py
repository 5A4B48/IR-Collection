import plistlib
import sys
import pandas as pd


def get_install_history(filename):
    if sys.platform.startswith('darwin'):
        pl = plistlib.readPlist('/Library/Receipts/InstallHistory.plist')
        information = []
        for i in pl:
            date = i.date
            name = i.displayName
            proc_name = i.processName
            pack_id = i.packageIdentifiers
            version = i.displayVersion
            if len(pack_id) > 1:
                for i in pack_id:
                    single = [date, name, proc_name, i, version]
                    information.append(single)
            else:
                version = i.displayVersion
                single = [date, name, proc_name, pack_id[0], version]
                information.append(single)

        df = pd.DataFrame(information)
        print('Install history logged ')
    
    return df



# get_install_history('/Library/Receipts/InstallHistory.plist')




