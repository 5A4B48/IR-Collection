import psutil
import pandas as pd


def get_processes_info():
    print("Gathering processes and their information")
    list = []
    for proc in psutil.process_iter():
        try:
            pinfo = proc.as_dict(attrs=['pid', 'name', 'cmdline', 'open_files'])
            p_name = pinfo['name']
            p_pid = pinfo['pid']
            p_cmdline = pinfo['cmdline']
            if p_cmdline is not None:
                if len(p_cmdline) > 0:
                    p_cmdline = ' '.join(p_cmdline)
            p_open_files = pinfo['open_files']
            if p_open_files is not None:
                if len(p_open_files) > 0:
                    for i in p_open_files:
                        filename = i.path
                        pidinfo = [p_pid, p_name, p_cmdline, filename]
                        list.append(pidinfo)

        except psutil.NoSuchProcess:
            pass

    df = pd.DataFrame(list)
    return df
