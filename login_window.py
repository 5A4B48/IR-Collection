import plistlib
import pandas as pd


def get_login_window(filename):

    with open(filename, 'rb') as file:
            information = []
            pl = plistlib.load(file)
            user_name = pl['lastUserName']
            user_status = pl['lastUser']
            if hasattr(pl, "SHOWFULLNAME"):
                is_full = pl['SHOWFULLNAME']
            else:
                is_full = "N/A"
            guest_status = pl['GuestEnabled']
            single = [user_name, user_status, is_full, guest_status]
            information.append(single)
    df = pd.DataFrame(information)
    print('Login Window Info Logged ')
    return df








