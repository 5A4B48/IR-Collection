import pandas as pd
import sys
if sys.platform.startswith('linux') or sys.platform.startswith('darwin'):
    import pwd


def get_users():
    susers = []
    users = pwd.getpwall()
    for user in users:
        if user.pw_name.startswith("_"):
            pass
        else:
            name = user.pw_name,
            shell = user.pw_shell
            uid = user.pw_uid
            gid = user.pw_gid
            single = [name, shell, uid, gid]
            susers.append(single)

    df = pd.DataFrame(susers)
    print("Logging User Info ")
    return df

