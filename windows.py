import os
import shutil
import ctypes
import sys
from connections import get_active_connections
from network_card_write import get_NIC_info
from processes import get_processes_info
from compression import make_zip
import pandas as pd



temp = os.getenv('UserProfile')


def gen_windows_artifacts(parent_dir):
    print('Windows OS detected\n')
    if ctypes.windll.shell32.IsUserAnAdmin() == 0:
        print('Script must be run with escalated privileges.')
        sys.exit(1)
    else:
        print('Starting to gather artifacts')
        if not os.path.exists(parent_dir):
            os.makedirs(parent_dir)
        if os.path.isdir(parent_dir + 'logs'):
            shutil.rmtree(parent_dir + 'logs')
        shutil.copytree('C:\Windows\System32\winevt\Logs', parent_dir + 'logs')
        processes = get_processes_info()
        connections = get_active_connections()
        network_interfaces = get_NIC_info()
        writer = pd.ExcelWriter(parent_dir + 'IR_Artifact.xlsx')
        processes.to_excel(writer, "Process List", index=False, header=['PID', 'Name ', 'Command Line', 'Open Files'])
        connections.to_excel(writer, "Connections List",index=False,
                                    header=['PID', 'Local Address', 'Local Port', 'Remote Address',
                                            'Remote Port', 'status'])
        network_interfaces.to_excel(writer, "Network Card Info",index=False,
                                   header=['NIC', 'Address', 'Family', 'netmask', 'broadcast', 'ptp', 'Running',
                                           'duplex', 'speed', 'mtu'])
        writer.save()
        make_zip(temp + "\Desktop\IRCollection", parent_dir)
        shutil.rmtree(parent_dir)
