import pandas as pd
import psutil


def get_NIC_info():
    print("Getting all information I can on Network interfaces.")
    network_devices = []
    connections = psutil.net_if_addrs()
    status = psutil.net_if_stats()
    for connection in connections:
        for i in range(len(connections.get(connection))):
            con = connections.get(connection)[i]
            stat = status.get(connection)
            nic = connection
            addrFamily = con.family
            address = con.address
            netmask = con.netmask
            broadcast = con.broadcast
            ptp = con.ptp
            run_status = stat.isup
            duplex_status = stat.duplex
            speed_status = stat.speed
            mtu_status = stat.mtu
            single = [nic, address, addrFamily, netmask, broadcast, ptp, run_status, duplex_status, speed_status, mtu_status]
            network_devices.append(single)

    df = pd.DataFrame(network_devices)
    return df
