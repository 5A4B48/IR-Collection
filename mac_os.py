import os
from shutil import copyfile
import shutil
from processes import get_processes_info
from connections import get_active_connections
from network_card_write import get_NIC_info
from login_window import get_login_window
from compression import make_zip
from Install_history import get_install_history
from get_os_update_info import get_os_update_info
from get_users import get_users
import pandas as pd
import sys


zip_file = '/tmp/IR_Collection'
process_headers = ['PID', 'Name ', 'Command Line', 'Open Files']
connection_headers = ['PID', 'Local Address', 'Local Port', 'Remote Address', 'Remote Port', 'status']
network_if_headers = ['NIC', 'Address', 'Family', 'netmask', 'broadcast', 'ptp', 'Running','duplex', 'speed', 'mtu']
install_hist_headers = ['Date', 'Name', 'Process Name', 'Package ID', 'Package Version']
login_window_headers = ['User Name', 'User Status', 'Is full name', 'Guest Status']
os_update_headers = ['Date', 'Status', 'Version', 'Build', 'Background Success Date']
users_headers = ['Name', 'Shell', 'UID', 'GID']

def mac_os(parent_dir):
    print('MAC OS Detected')
    if os.getuid() != 0:
        print('Script must be run as root, please sudo')
        sys.exit(1)
    else:
        if not os.path.exists(parent_dir):
            os.makedirs(parent_dir)
        if os.path.isdir(parent_dir + 'logs'):
            shutil.rmtree(parent_dir + 'logs')
        copyfile('/etc/passwd', parent_dir + "passwd")
        shutil.copytree('/var/log', parent_dir + 'logs')
        processes = get_processes_info()
        connections = get_active_connections()
        network_interfaces = get_NIC_info()
        login_window = get_login_window('/Library/Preferences/com.apple.loginwindow.plist')
        os_update = get_os_update_info('/Library/Preferences/com.apple.SoftwareUpdate.plist')
        users = get_users()
        install_history = get_install_history('/Library/Receipts/InstallHistory.plist')
        writer = pd.ExcelWriter(parent_dir + 'IR_Artifact.xlsx')
        processes.to_excel(writer, "Process List", index=False, header=process_headers)
        connections.to_excel(writer, "Connections List", index=False,header= connection_headers)
        network_interfaces.to_excel(writer, "Network Card Info", index=False,header= network_if_headers)
        install_history.to_excel(writer, 'Install History', index=False, header=install_hist_headers)
        login_window.to_excel(writer, 'Login Window', index=False, header= login_window_headers)
        os_update.to_excel(writer, 'OS Update Info', index=False, header= os_update_headers)
        users.to_excel(writer, "Users", index=False, header= users_headers)
        writer.save()
        make_zip(zip_file, parent_dir)
        shutil.rmtree(parent_dir)
