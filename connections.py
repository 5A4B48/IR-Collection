import pandas as pd
import psutil


def get_active_connections():
    print("Gathering Network Connections")
    network_connections = []
    connections = psutil.net_connections()
    for connection in connections:
        pid = connection.pid
        if hasattr(connection.laddr, 'ip'):
            local_ip = connection.laddr.ip
        else:
            local_ip = ''
        if hasattr(connection.laddr, 'port'):
            local_port = connection.laddr.port
        else:
            local_port = ''
        if hasattr(connection.raddr, 'ip'):
            remote_ip = connection.raddr.ip
        else:
            remote_ip = ''
        if hasattr(connection.raddr, 'port'):
            remote_port = connection.raddr.port
        else:
            remote_port = ''
        status = connection.status
        if status !="NONE":
            single = [pid, local_ip,local_port,remote_ip, remote_port, status]
            network_connections.append(single)

    # print(connections, filename)
    df = pd.DataFrame(network_connections)
    return df
