import plistlib
import pandas as pd


def get_os_update_info(filename):

    with open(filename, 'rb') as file:
            information = []
            pl = plistlib.load(file)
            successful_date = pl['LastFullSuccessfulDate']
            status = pl['LastSessionSuccessful']
            version = pl['LastAttemptSystemVersion']
            build = pl['LastAttemptBuildVersion']
            if hasattr(pl, "LastBackgroundSuccessfulDate"):
                background_success = pl['LastBackgroundSuccessfulDate']
            else:
                background_success = "N/A"

            single = [successful_date, status, version, build, background_success]
            information.append(single)

    df = pd.DataFrame(information)
    print('OS Update Info Logged ')
    return df



