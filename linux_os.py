import os
import sys
from shutil import copyfile
import shutil
from processes import get_processes_info
from connections import get_active_connections
from network_card_write import get_NIC_info
from compression import make_zip
import pandas as pd
from get_users import get_users

zip_file = '/tmp/IR_Collection'
process_headers = ['PID', 'Name ', 'Command Line', 'Open Files']
connection_headers = ['PID', 'Local Address', 'Local Port', 'Remote Address', 'Remote Port', 'status']
nic_headers = ['NIC', 'Address', 'Family', 'netmask', 'broadcast', 'ptp', 'Running', 'duplex', 'speed', 'mtu']
user_headers = ['Name', 'Shell', 'UID', 'GID']

def linux_os(parent_dir):
    print('Linux OS Detected')
    if os.getuid() != 0:
        print('Script must be run as root, please sudo')
        sys.exit(1)
    else:
        if not os.path.exists(parent_dir):
            os.makedirs(parent_dir)
        if os.path.isdir(parent_dir + 'logs'):
            shutil.rmtree(parent_dir + 'logs')
        copyfile('/etc/passwd', parent_dir + "passwd")
        copyfile('/etc/fstab', parent_dir + "fstab")
        shutil.copytree('/var/log', parent_dir + 'logs')
        if os.path.exists('/var/lib/iptables/'):
            shutil.copytree('/var/lib/iptables/', parent_dir + 'iptable_information')
        processes = get_processes_info()
        connections = get_active_connections()
        network_interfaces = get_NIC_info()
        users = get_users()

        writer = pd.ExcelWriter(parent_dir + 'IR_Artifact.xlsx')
        processes.to_excel(writer, "Process List", index=False, header=process_headers)
        connections.to_excel(writer, "Connections List", index=False,
                             header=connection_headers)
        network_interfaces.to_excel(writer, "Network Card Info", index=False,
                                    header=nic_headers)
        users.to_excel(writer, "Users", index=False, header=user_headers)
        writer.save()
        make_zip(zip_file, parent_dir)
        shutil.rmtree(parent_dir)
