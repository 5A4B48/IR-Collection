import sys
import os
from mac_os import mac_os
from windows import gen_windows_artifacts
from linux_os import linux_os


artifact_directory = '/tmp/artifacts/'

banner = """
   _____                .__                        .__                .__          __   
  /  _  \_______   ____ |  |__ _____    ____  ____ |  |   ____   ____ |__| _______/  |_ 
 /  /_\  \_  __ \_/ ___\|  |  \\\__  \ _/ __ \/  _ \|  |  /  _ \ / ___\|  |/  ___/\   __\
/    |    \  | \/\  \___|   Y  \/ __ \\  ___(  <_> )  |_(  <_> ) /_/  >  |\___ \  |  |  
\____|__  /__|    \___  >___|  (____  /\___  >____/|____/\____/\___  /|__/____  > |__|  
        \/            \/     \/     \/     \/                 /_____/         \/        
       
       
       Beginning to dig for artifacts, will begin by attempting to
       detect your operating system.                                                             

"""

print(banner)

if sys.platform.startswith('linux'):
    linux_os(artifact_directory)

elif sys.platform.startswith('win32'):
    artifact_directory = os.getenv('temp') + '\\artifacts\\'
    gen_windows_artifacts(artifact_directory)

elif sys.platform.startswith('darwin'):
    mac_os(artifact_directory)






