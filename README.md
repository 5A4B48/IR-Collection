[![Updates](https://pyup.io/repos/github/5A4B48/IR-Collection/shield.svg)](https://pyup.io/repos/github/5A4B48/IR-Collection/)
[![Python 3](https://pyup.io/repos/github/5A4B48/IR-Collection/python-3-shield.svg)](https://pyup.io/repos/github/5A4B48/IR-Collection/)

# Archaeologist
Cross-platform IR artifact harvester


## Building with pyinstaller

### On windows
pyinstaller --onefile --distpath=Z:\Downloads\dist --workpath=Z:\Downloads\work  Z:\IR-Collection\archaeologist.py --hidden-import=pandas._libs.tslibs.timedeltas
