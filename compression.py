from shutil import make_archive
import os



def make_zip(output_filename, source_dir):
    print('Zipping files...')
    if os.path.isfile(output_filename):
        os.remove(output_filename)
    make_archive(output_filename, "zip", source_dir)
    print('Artifacts gathered and stored at ' + output_filename + '.zip')

